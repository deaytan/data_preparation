#!/usr/bin/env/python
import getopt, sys
import warnings


if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]

unixOptions = "t:h"
gnuOptions = ["truth=","help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-t --truth = True output results")
		print("-h --help = show the help message (have fun!))")
		sys.exit()

def main():

	##This script is generated for a species case
	##The input data should include multi resistance information
	##The output file produces will include multi outputs as well. 
	##The profiles relies on the defined resistance profiles.

	import numpy as np
	import collections

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-t","--truth"):
			true_path = currentValue

	tb_data = np.loadtxt("%s" % true_path, dtype = "string", delimiter=",")

	##output files
	rif = open("RIFAMPICIN.txt", "w")
	iso = open("ISONIAZID.txt", "w")
	strp = open("STREPTOMYCIN.txt", "w")
	eth = open("ETHAMBUTOL.txt", "w")
	pyr = open("PYRAZINAMIDE.txt", "w")
	cip = open("CIPROFLOXACIN.txt", "w")
	anb = open("six_antibiotics.txt", "w")

	ant_dic = collections.defaultdict(list)

	sample_list = [] 

	for each in tb_data:
		sample_list.append(each[0])

	sample_list = list(set(sample_list))
	
	for s in sample_list: 
		a = []
		for each in tb_data:
			if each[0] == s:
				if each[1].lower() == "rifampicin":
					rif.write(each[0])
					rif.write("\t")
					if "sus" in each[2].lower():
						rif.write("0")
						rif.write("\n")
						a.append("negative1")
					else:
						rif.write("1")
						rif.write("\n")
						a.append("positive1")
					ant_dic[each[1]].append("p")
				if each[1].lower() == "isoniazid":
					iso.write(each[0])
					iso.write("\t")
					if "sus" in each[2].lower():
						iso.write("0")
						iso.write("\n")
						a.append("negative2")
					else:
						iso.write("1")
					    iso.write("\n")
						a.append("positive2")
					ant_dic[each[1]].append("p")
				if each[1].lower() == "streptomycin":
					strp.write(each[0])
					strp.write("\t")
					if "sus" in each[2].lower():
						strp.write("0")
						strp.write("\n")
						a.append("negative3")
					else:
						strp.write("1")
					    strp.write("\n")
						a.append("positive3")
					ant_dic[each[1]].append("p")
				if each[1].lower() == "ethambutol":
					eth.write(each[0])
					eth.write("\t")
					if "sus" in each[2].lower():
						eth.write("0")
						eth.write("\n")
						a.append("negative4")
					else:
						eth.write("1")
						eth.write("\n")
						a.append("positive4")
					ant_dic[each[1]].append("p")
				if each[1].lower() == "pyrazinamide":
					pyr.write(each[0])
					pyr.write("\t")
					if "sus" in each[2].lower():
						pyr.write("0")
						pyr.write("\n")
						a.append("negative5")
					else:
						pyr.write("1")
					 	pyr.write("\n")
						a.append("positive5")
					ant_dic[each[1]].append("p")
				if each[1].lower() == "ciprofloxacin":
					cip.write(each[0])
					cip.write("\t")
					if "sus" in each[2].lower():
						cip.write("0")
						cip.write("\n")
						a.append("negative6")
					else:
						cip.write("1")
					 	cip.write("\n")
						a.append("positive6")
					ant_dic[each[1]].append("p")
				else:
					ant_dic[each[1]].append("p")
		if len(list(set(a))) > 6:
			print(list(set(a)))
		anb.write(s)
		anb.write("\t")
		if "positive1" in a:
			anb.write("1")
			anb.write("\t")
		elif "negative1" in a:
			anb.write("0")
			anb.write("\t")
		elif "positive1" not in a and "negative1" not in a:
			anb.write("-1")
			anb.write("\t")
		if "positive2" in a:
			anb.write("1")
			anb.write("\t")
		elif "negative2" in a:
			anb.write("0")
			anb.write("\t")
		elif "positive2" not in a and "negative2" not in a:
			anb.write("-1")
			anb.write("\t")
		if "positive3" in a:
			anb.write("1")
			anb.write("\t")
		elif "negative3" in a:
			anb.write("0")
			anb.write("\t")
		elif "positive3" not in a and "negative3" not in a:
			anb.write("-1")
			anb.write("\t")
		if "positive4" in a:
			anb.write("1")
			anb.write("\t")
		elif "negative4" in a:
			anb.write("0")
			anb.write("\t")
		elif "positive4" not in a and "negative4" not in a:
			anb.write("-1")
			anb.write("\t")
		if "positive5" in a:
			anb.write("1")
			anb.write("\t")
		elif "negative5" in a:
			anb.write("0")
			anb.write("\t")
		elif "positive5" not in a and "negative5" not in a:
			anb.write("-1")
			anb.write("\t")
		if "positive6" in a:
			anb.write("1")
			anb.write("\n")
		elif "negative6" in a:
			anb.write("0")
			anb.write("\n")
		elif "positive6" not in a and "negative6" not in a:
			anb.write("-1")
			anb.write("\n")

	rif.close()
	iso.close()
	strp.close()
	eth.close()
	pyr.close()
	cip.close()
	anb.close()

if __name__ == '__main__':
    main()	
