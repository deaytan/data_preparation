#!/usr/bin/env/python
import getopt, sys
import warnings


if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]

unixOptions = "p:r:h"
gnuOptions = ["pointfin=", "resfin=","help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-p --pointfin = PointFinder results")
		print("-r --resfin = ResFinder results")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():

	##this script aims to merge pointfinder data and resfinder data into a file

	import numpy as np

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-p","--pointfin"):
			point_path = currentValue

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-r","--resfin"):
			res_path = currentValue


	snps = np.loadtxt("%s" % point_path, dtype = "string")
	genes = np.loadtxt("%s" % res_path, dtype= "string")

	uniq_genes = []
	for each in genes[:,1]:
		uniq_genes.append(each)

	uniq_genes = list(set(uniq_genes))

	file_w = open("output_res_point.txt", "w")
	for each in snps:
		for e in each:
			file_w.write(e)
			file_w.write("\t")
		if each[0] in genes[:,0]:
			gene_index = [i for i, x in enumerate(genes[:,0]) if x == each[0]]
			acquired_genes = []
			coverage = []
			for g in gene_index:
				tem = []
				acquired_genes.append(uniq_genes.index(genes[g,1]))
				coverage.append(genes[g, 2])
			
			gap = 0	
			acquired_genes_uniq = list(set(acquired_genes))
			for a in sorted(acquired_genes_uniq):
				b = a - gap		
				for i in range(0, b):
					file_w.write("-1")
					file_w.write("\t")
				ind = acquired_genes.index(a)
				file_w.write(str(coverage[ind]))
				file_w.write("\t")
				gap = a + 1
			if sorted(acquired_genes)[-1] != len(uniq_genes) - 1:
				for m in range(sorted(acquired_genes)[-1] + 1, len(uniq_genes)):
					file_w.write("-1")
					file_w.write("\t")
		else:
			for i in range(0, len(uniq_genes)):
				file_w.write("-1")
				file_w.write("\t")
		file_w.write("\n")
	file_w.close()

if __name__ == '__main__':
    main()	