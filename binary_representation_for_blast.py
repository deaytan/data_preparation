#!/usr/bin/env/python
import getopt, sys
import warnings


if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]

unixOptions = "p:h"
gnuOptions = ["points=","help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-p --points = PointFinder results")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():

	##this script analyses all the pointfinder results 
	##this script produces an output file including all the mutated positions. 
	##According to the absence presence of the mutation, each feature is marked as 1 or 0. 
	##What this script does not provide that which kind of mutation has? all the mutations are taken into consideration equally.
	##Last updated ; 04.03.2019

	import os
	import collections
	import re
	import numpy as np

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-p","--points"):
			input_path = currentValue

	###sample list desired to include into the dataset###
	file_dir = os.listdir("%s" % input_path)

	##be sure there is no undesired file
	points = []
	for each in file_dir:
		if ".sh" not in each and ".py" not in each and ".txt" not in each and ".png" not in each:
			points.append(each)

	dict_mutations = collections.defaultdict(list)
	all_samples = collections.defaultdict(list)
	all_aa = []
	for sample in points:
		list_point = os.listdir("%s/%s" % (input_path, sample))
		for item in list_point:
			if "PointFinder_results.txt" in item:
				data_tsv = open("%s/%s/%s" % (input_path, sample, item), "r")
				tsv = data_tsv.readlines()
				temp_dict = collections.defaultdict(list)
				for each in tsv[1:]:
					each = each.replace(" promotor", "")
					each = each.replace(" promoter", "")
					splitted1 = each.split("\t")[0]
					splitted = splitted1.split(" ")[0:2]
					position = re.findall("(\-?\d+)", splitted[1])
					dict_mutations[splitted[0].lower()].append(position[0])
					temp_dict[splitted[0].lower()].append(position[0])
				all_samples[sample].append(temp_dict)


		
	results = open("pointfinder_chr_mutations.txt", "w")

	dict_mutations2 = collections.defaultdict(list)

	for each in dict_mutations:
		uniq_res = list(set(list(dict_mutations[each])))
		dict_mutations2[each].append(uniq_res)

	all_genes = []
	for each in dict_mutations2:
		all_genes.append(each)
 
	for k in all_samples:
		results.write(str(k))
		results.write("\t")
		for g in all_genes:  
			if g in list(all_samples[k][0]):
				for each in dict_mutations2[g][0]:
					if each in all_samples[k][0][g]:
						results.write("1")
						results.write("\t")
					else:
						results.write("-1")
						results.write("\t")
			else:
				if g in dict_mutations2:
					for a in dict_mutations2[g][0]:
						results.write("-1")
						results.write("\t")
		results.write("\n")
		
	results.close()

if __name__ == '__main__':
    main()	