#!/usr/bin/env/python
import getopt, sys
import warnings


if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]

unixOptions = "t:b:a:rsdh"
gnuOptions = ["true=","break=","anti=","int2res", "int2sus", "discard", "help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-t --true = True output files")
		print("-b --break = Break point/points (format ex: 0.05_1_0.08)")
		print("-a --anti = Antibiotic name/names (format ex: ciprofloxacin_ethambutol_rifampicin)")
		print("-r --int2res = Intermediate samples should be counted as resistant")
		print("-s --int2sus = Intermediate samples should be counted as susceptible")
		print("-d --discard = Intermediate samples should be discarded")
		print("-h --help = show the help message (have fun!)")
		sys.exit()


def main():

	##This script produces binary outputs based on the already decided profiles and MIC values. 
	##Outputs would be epidemiological or clinical profiles, that depends on the break points/cutoffs given
	##The user should be careful about the already given profiles. 

	import re

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-t","--true"):
			true_path = currentValue
	

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-b","--break"):
			break_point = str(currentValue)


	for currentArgument, currentValue in arguments:
		if currentArgument in ("-a","--anti"):
			antibiotic = str(currentValue)
            
   	#number of antibiotics
    
	antibiotics = antibiotic.split("_")
    
	n_antibiotics = len(antibiotics)
	
	breaks = break_point.split("_")

	data_open = open("%s" % true_path, "r")

	data_read = data_open.readlines()

	out = open("output_profiles.txt", "w")

	for each in data_read:
		ID = each.split()[0]
		out.write(ID)
        	out.write("\t")	
		for a in range(n_antibiotics):
			if antibiotics[a].lower() in each.lower():
				if "breakpoint" in each.lower():
					part_1 = each.split()[-1]
					val = float(part_1.split("mg/l")[0])
					if "susce" in each.lower():
						if val <= float(breaks[a]):
							out.write("0")
							out.write("\t")
					elif "resis" in each.lower():
						if val > float(breaks[a]):
							out.write("1")
							out.write("\t")
				elif "mg/L" in each or "mg/l" in each:
					part1 = each.split("%s" % antibiotic)[1]
					part2 = part1.split()[0]
					part3 = part1.split()[1]
					part4 = part1.split()[2]
				
					if part2.isalpha() == False:
						if "." in str(part2): 
						    	#val = re.findall("\d+.\d+", part2)[0]
							val = str(float(part4))
						else:
						    val = re.findall("\d+", part2)[0]
					elif part3.isalpha() == False:
					    if "." in str(part3):
						    val = re.findall("\d+.\d+", part3)[0]
						
					    else:
						    val = re.findall("\d+", part3)[0]
					else:
					    if "." in str(part4):
						    val = re.findall("\d+.\d+", part4)[0]
					    else:
						    val = re.findall("\d+", part4)[0]				
				
					if float(val) < float(breaks[a]):
						out.write("0")
						out.write("\t")
					else:
						out.write("1")
						out.write("\t")
				else:
					if "Resis" in each or "resis" in each:
						out.write("1")
						out.write("\t")
					elif "Susce" in each or "susce" in each:
						out.write("0")
						out.write("\t")
					else:
					    for currentArgument, currentValue in arguments:
						if currentArgument in ("-d","--discard"):
							out.write("-1")
							out.write("\t")
						if currentArgument in ("-r", "int2res"):
							out.write("1")
							out.write("\t")
						if currentArgument in ("-s", "int2sus"):
							out.write("0")
							out.write("\t")
                                
			else:  
				out.write("-1")
				out.write("\t")
		out.write("\n")                
	out.close()

if __name__ == '__main__':
    main()	