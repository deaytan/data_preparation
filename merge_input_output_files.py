#!/usr/bin/env/python
import getopt, sys
import warnings


if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]

unixOptions = "i:o:h"
gnuOptions = ["input=", "output=","help"]

try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-i --input = Input file")
		print("-o --output = Output file")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():

	##input file should include sample ID and features per sample
	##output file should include sample ID and output(s) per sample
	##the script output is the same ordered input and output files without sample IDS, and sample names 
	##will be produced as a separate file. 

	import numpy as np

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-i","--input"):
			input_path = currentValue


	for currentArgument, currentValue in arguments:
		if currentArgument in ("-o","--output"):
			output_path = currentValue

	data_x = np.loadtxt("%s" % input_path, dtype="string")

	data_y = np.loadtxt("%s" % output_path, dtype="string")

	data_y_list = [] 

	columns = len(data_y[0])

	for each in data_y:
		data_y_list.append(each[0])


	data_x_new = open("data_x.txt", "w")
	data_y_new = open("data_y.txt", "w")
	sample_list = []
	all_names = open("data_names.txt", "w")
	for each in data_x[0:]:
		if each[0] in data_y_list:
			ind = data_y_list.index(each[0])
			sample_list.append(each[0])
			all_names.write(str(each[0]))
			all_names.write("\n")
			for i in each[1:]:
				data_x_new.write(i)
				data_x_new.write("\t")
			data_x_new.write("\n")
			for i in  range(1,columns):
				data_y_new.write(data_y[ind,i])
				if i != columns-1:
					data_y_new.write("\t")
				elif i == columns-1:
					data_y_new.write("\n")


	data_x_new.close()
	data_y_new.close()
	all_names.close()

if __name__ == '__main__':
    main()