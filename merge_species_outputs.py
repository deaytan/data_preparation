#!/usr/bin/env/python
import getopt, sys
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]

unixOptions = "e:t:s:p:h"
gnuOptions = ["ecoli=", "tuber=","salmo=", "stap=", "help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-e --ecoli = E. coli input")
		print("-t --tuber = M. tuberculosis input")
		print("-s --salmo = S. enterica input")
		print("-p --stap = S. aureus input")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():

	##this script four different species.
	##this script is generated for a specific case:
	##TB has six outputs,
	##others have 5 missing features in the outputs. 
	##species output files are required.
	##output file is a merged output file with sample IDs. 

	import numpy as np

	###species#####

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-e","--ecoli"):
			ecoli_path = currentValue

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-t","--tuber"):
			tb_path = currentValue

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-s","--salmo"):
			sal_path = currentValue

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-p","--stap"):
			sta_path = currentValue

	###species#####

	###tuberculosis###
	tb_out = np.loadtxt("%s" % tb_path, dtype = "string")

	###E.coli####
	coli_out = np.loadtxt("%s" % ecoli_path, dtype = "string")

	###Salmonella###
	sal_out = np.loadtxt("%s" % sal_path, dtype = "string")

	###Staphylococcus
	stap_out = np.loadtxt("%s" % sta_path, dtype = "string")


	coli_n = len(coli_out)
	tb_n = len(tb_out)
	sal_n = len(sal_out)
	stap_n = len(stap_out)

	total_n = coli_n + tb_n + sal_n + stap_n

	dist = ["coli"]*coli_n + ["tb"]*tb_n + ["sal"]*sal_n + ["stap"]*stap_n


	new_file_out = open("tb_ecoli_sal_stap_binary_outputs.txt", "w")

	for each in range(total_n):
		if dist[each] == "coli":
			new_file_out.write(str(coli_out[each][0]))
			new_file_out.write("\t")
			for m in range(5):
				new_file_out.write(str(-1))
				new_file_out.write("\t")

			new_file_out.write(str(coli_out[each][1]))
			new_file_out.write("\n")

		elif dist[each] == "tb":
			new_file_out.write(str(tb_out[each-coli_n][0]))
			new_file_out.write("\t")
			for m in [1,2,3,4,5,6]:
				new_file_out.write(str(tb_out[each-coli_n][m]))
				new_file_out.write("\t")
			new_file_out.write("\n")



		elif dist[each] == "sal":
			new_file_out.write(str(sal_out[each-coli_n-tb_n][0]))
			new_file_out.write("\t")
			for m in range(5):
				new_file_out.write(str(-1))
				new_file_out.write("\t")
			
			new_file_out.write(str(sal_out[each-coli_n-tb_n][1]))
			new_file_out.write("\n")

		elif dist[each] == "stap":
			new_file_out.write(str(stap_out[each-coli_n-tb_n-sal_n][0]))
			new_file_out.write("\t")
			for m in range(5):
				new_file_out.write(str(-1))
				new_file_out.write("\t")
			
			new_file_out.write(str(stap_out[each-coli_n-tb_n-sal_n][1]))
			new_file_out.write("\n")

	new_file_out.close()

if __name__ == '__main__':
    main()	
		