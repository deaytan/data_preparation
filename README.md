# Data preparation

All the programs are written using Python 2.7. 

# Analyse PointFinder outputs

## binary_representation.py, scored_representation.py, nucleotide_representation.py and amino_acid_representation.py 

These Python scripts analyse the PointFinder outputs.

For all the programs, required arguments:

-p --points = PointFinder results

-h --help = show the help message (have fun!))

This argument only specific to the scored representation that merges the scored and binary representation types. 

-m --mutcol = Add the binary mutation column

Please note: The PointFinder result folders should be called exactly the same with the .tsv folders. 

For instance: If the folder name including Pointfinder outputs is SRR7818258, the .tsv file should be named: SRR7818258_kma_results.tsv.

### Running the programs

python scored_representation.py -p /path/to/PointFinder/results [options]

# ResFinder_analyser.py

This script analyses the ResFinder results by fetching information regarding absence/presence of acquired genes.

Required arguments:

-r --respath = path to the ResFinder results 

-h --help = show the help message (have fun!))

### Running the program 

python ResFinder_analyser.py -r /path/to/ResFinder/results 

# merge_resfinder_pointfinder.py 

This script merges the PointFinder and ResFinder results into one file. 

Required arguments:

-p --pointfin = PointFinder results

-r --resfin = ResFinder results

-h --help = show the help message (have fun!))

### Running the program 

python merge_resfinder_pointfinder.py -p /path/to/PointFinder/results -r /path/to/ResFinder/results 

# merge_input_output_files.py 

The input and ouput files do not have the same sample order and complete resistance profiles of interested antimicrobials, in that case input and output files should be matched. 
That process is completed using this script. 

Required arguments:

-i --input = Input file

-o --output = Output file

-h --help = show the help message (have fun!))

Input file is the output of the analysed PointFinder results.

Output file is the true resistance profiles. 

### Running the program

python merge_input_output_files.py -i /path/to/input/file -o /path/to/output/file

# merge_scaffolds.py 

This script merges the SPAdes program scaffolds.fasta outputs with blank spacers. The output is one long sequence including multiple samples, used as input by the kma_clustering program. 

Required arguments:

-p --path = Path to SPAdes results

-s --space = Space between the assemblies

-h --help = show the help message (have fun!))

### Running the program 

python merge_scaffolds.py -p /path/to/SPAdes/results -s [number]

# merge_species.py 

This program merges multi species input and output data into two separate files. Please note, each species input data is generated separetely, and merged later.

Required arguments:

-e --ecoli = E. coli input

-t --tuber = M. tuberculosis input

-s --salmo = S. enterica input

-p --stap = S. aureus input

-a --eout = E. coli output

-b --tout = M. tuberculosis output

-c --sout = S. enterica output

-d --staout = S. aureus output

-h --help = show the help message (have fun!))

### Running the program 
 
python merge_species.py -e /path/to/E_coli/input -t /path/to/M_tuberculosis/input -s /path/to/S_enterica/input -p /path/to/S_aureus/input -a /path/to/E_coli/output -b /path/to/M_tuberculosis/output -c /path/to/S_enterica/output -d /path/to/S_aureus/output

# prepare_multi_output.py

This script prepares multi-output data in case of multiple-resistance. The script consideres the profiles already detected.
The first column of the output file is the sample IDs, other columns have binary inputs, 1 corresponds to resistance, 0 susceptible. Missing profiles are marked "-1".

Required arguments:

-t --truth = True output results

-h --help = show the help message (have fun!))

### Running the program

python prepare_multi_output.py -t /path/to/multi_resistance/profiles 

# prepare_output.py 

This script provides multi or single output data considering already determined profiles and MIC values. 

The first column of the output file is the sample IDs, other columns include resistance profiles in binary format where 1 corresponds to resistance, 0 susceptible. Missing profiles are marked "-1".

The program requires epidemiological cut-offs or clinical brekapoints of interested antibiotics. 

### Program options

-t --true = True output files

-b --break = Break points

-a --anti = Antibiotic name/names (format ex: ciprofloxacin_ethambutol_rifampicin)

-r --int2res = Intermediate samples should be counted as resistant

-s --int2sus = Intermediate samples should be counted as susceptible

-d --discard = Intermediate samples should be discarded

-h --help = show the help message (have fun!)

### Running the program

python prepare_output.py -t /path/to/multi_resistance/profiles -b [number] -a [name] -r|-s|-d

# Predictions_ResFinder_PointFinder_tools.py

This script analyses Point-/ResFinder tools prediction results and produce MCC, sensitivity, specifity and F1 scores.

The predictions will be produced for the validation data and the remaining test data. 

The program requires the correct resistance profiles for the antibiotic interested, all the sample names used and the validation names. 

The files including sample names should follow the format below:

For example:

1234.56\n2345.77\n2345.88

### Program options

-r --respath = path to ResFinder results

-p --pointpath = path to PointFinder results

-t --true = path to true outputs

-f --file = path to samples used in the study

-v --validation = path to validation samples

-a --antibiotic = antibiotic name interested

-i --include = consider validation samples

-d --disclude = consider test samples

-h --help = show the help message (have fun!)

### Running the program

python Predictions_ResFinder_PointFinder_tools.py -p /path/to/PointFinder/results -r /path/to/ResFinder/results -t /path/to/true/outputs -f /path/to/file -v /path/to/validation/samples -a [string] -i|-d 

# kma_clustering.c

This program is written by Philip TLC Clausen and it clusters samples based on the genome similarities. The program needs to be compilled using the following command line:

gcc -O3 -o kma_clustering kma_clustering.c -lm -lz

mv kma_clustering ~/bin/

The program uses assemblies merged as input. Assembly merging can be done using merge_scaffolds.py. 

### Program options 

Options are:	|	Desc:					          |      Default:
-i		|  Input/query file name (STDIN: "--")        |       None
-o		| Output file				                  |      Input/template file
-batch	| Batch input file
-deCon	|  File with contamination (STDIN: "--")	  |          None/False
-batchD	|  Batch decon file
-t_db	|  Add to existing DB			              |     None/False
-k		|  Kmersize				                      |      16
-k_t	|  Kmersize for template identification	      |      16
-k_i	|  Kmersize for indexing			          |          16
-ML		|  Minimum length of templates		          |      kmersize (16)
-CS		|  Start Chain size			                  |      1 M
-ME		|  Mega DB					                  |      False
-NI		|  Do not dump *.index.b			          |          False
-Sparse	|  Make Sparse DB ('-' for no prefix)	      |      None/False
-ht		|  Homology template			              |          1.0
-hq		|  Homology query				              |      1.0
-and	|  Both homolgy thresholds has to be reached	|        or
-v		|  Version
-h		|  Shows this help message
 
### Running the program

cat /path/to/merged/assembly |kma_clustering -i -- - [options]

# Dependencies

## Required Python Packages

* getopt
* sys
* warnings
* matplotlib
* os
* numpy
* sklearn
* pandas
* scipy
* collections
* random
* re
* Bio


