#!/usr/bin/env/python
import getopt, sys
import warnings


if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]

unixOptions = "r:h"
gnuOptions = ["respath=","help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-r --respath = path to ResFinder results")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():

	####this script finds all the acquired genes and returns an output file

	import os
	import numpy as np

	for currentArgument, currentValue in arguments:
			if currentArgument in ("-r","--respath"):
				input_path = str(currentValue)

	file_list = os.listdir("%s" % input_path)

	output = open("resfinder_acquired_genes.txt", "w")

	for each in file_list:
		if ".txt" not in each and ".sh" not in each and ".py" not in each:
			aq_open = open("%s/%s/ResFinder_results_tab.txt" % (input_path, str(each)), "r")
			aq = aq_open.readlines()
			for i in range(1,len(aq)):
				output.write(str(each))
				output.write("\t")
				output.write(aq[i].split("\t")[0])
				output.write("\t")	
				output.write(str(float(aq[i].split("\t")[3])/100))
				output.write("\n")
			aq_open.close()


	output.close()

if __name__ == '__main__':
    main()