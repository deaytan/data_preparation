import getopt, sys
import warnings


if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]

unixOptions = "r:p:t:f:v:a:idh"
gnuOptions = ["respath=","pointpath=", "true=", "file=", "validation=", "antibiotic=", "include", "disclude", "help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-r --respath = path to ResFinder results")
		print("-p --pointpath = path to PointFinder results")
		print("-t --true = path to true outputs")
		print("-f --file = path to samples used in the study")
		print("-v --validation = path to validation samples")
		print("-a --antibiotic = antibiotic name interested")
		print("-i --include = consider validation samples")
		print("-d --disclude = consider test samples")
		print("-h --help = show the help message (have fun!)")
		sys.exit()


def main():


	import os
	import numpy as np
	from collections import defaultdict
	from sklearn.metrics import matthews_corrcoef
	from sklearn.metrics import confusion_matrix
	from sklearn.metrics import classification_report
	from sklearn.metrics import f1_score

	for currentArgument, currentValue in arguments:
				if currentArgument in ("-p","--pointpath"):
					path_to_pointfinder = str(currentValue)
				if currentArgument in ("-r","--respath"):
					path_to_resfinder = str(currentValue)
				if currentArgument in ("-t","--true"):
					path_to_true = str(currentValue)
				if currentArgument in ("-f","--file"):
					path_to_file = str(currentValue)
				if currentArgument in ("-v", "--validation"):
					path_to_validation = str(currentValue)
				if currentArgument in ("-a", "--antibiotic"):
					antibiotic = str(currentValue)

	##the file names

	file_names = os.listdir("%s" % path_to_pointfinder)


	pointfinder_new_res = open("pointfinder_new_results.txt", "w")
	for each in file_names:
		if ".txt" not in each and ".py" not in each and ".sh" not in each and "resfinder4" not in each:
			name = each.split(".")[0]
			point = open("%s/%s/%s_kma_prediction.txt" % (path_to_pointfinder, each, name), "r")
			point = point.readlines()
			pointfinder_new_res.write(str(each))
			pointfinder_new_res.write("\t")	
			pointfinder_new_res.write(point[1].split()[3])
			pointfinder_new_res.write("\n")

	pointfinder_new_res.close()

	file_names = os.listdir("%s" % path_to_resfinder)

	pointfinder_new_res = open("resfinder_new_results.txt", "w")
	for each in file_names:
		if ".txt" not in each and ".py" not in each and ".sh" not in each:
			point = open("%s/%s/pheno_table.txt" % (path_to_resfinder, each), "r")
			point = point.readlines()
			name = point[1].split()[2][:-3]
			for line in point:
				if antibiotic in line:
					res = line.split()[2]
			pointfinder_new_res.write(str(name))
			pointfinder_new_res.write("\t")	
			pointfinder_new_res.write(str(res))
			pointfinder_new_res.write("\n")

	pointfinder_new_res.close()

	##true values

	truth = np.loadtxt("%s" % path_to_true, dtype = "string")

	dict_finder = defaultdict(list)

	####samples used 

	used = list(np.loadtxt("%s" % path_to_file, dtype = "string"))

	#######pointfinder results

	pointfinder_file_name = np.loadtxt("./pointfinder_new_results.txt", dtype = "string")

	#######resfinder results 

	resfinder_file_name = np.loadtxt("./resfinder_new_results.txt", dtype = "string")

	#######validation 
	validation = []
	validation_data = np.loadtxt("%s" % path_to_validation, dtype = "string")
	for each in validation_data:
		validation.append(each)

	for e in range(0, len(pointfinder_file_name)):
		name = pointfinder_file_name[e][0]
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-i","--include"):
				if name in validation and name in used:
					if "1" in pointfinder_file_name[e][1] or "0" in pointfinder_file_name[e][1]:
						dict_finder[name].append(int(pointfinder_file_name[e][1]))
					else:
						print(pointfinder_file_name[e][1])
			if currentArgument in ("-d","--disclude"):
				if name not in validation and name in used:
					if "1" in pointfinder_file_name[e][1] or "0" in pointfinder_file_name[e][1]:
						dict_finder[name].append(int(pointfinder_file_name[e][1]))
					else:
						print(pointfinder_file_name[e][1])

	for e in range(0, len(resfinder_file_name)):
		name = resfinder_file_name[e][0]
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-i","--include"):
				if name in validation and name in used:
					if "No" in resfinder_file_name[e]:
						dict_finder[name].append(0)
					else: 
						print(resfinder_file_name[e])
						dict_finder[name].append(1)
		
			if currentArgument in ("-d","--disclude"):
				if name not in validation and name in used:
					if "No" in resfinder_file_name[e]:
						dict_finder[name].append(0)
					else: 
						dict_finder[name].append(1)

	results_write = open("results.txt", "w")
		
	correct_results = []
	prediction_results = []
	for i in dict_finder:
		if len(dict_finder[i]) > 1 and i in list(truth[:,0]):
			ind = list(truth[:,0]).index(i)
			if "-" not in truth[ind,1]:
				correct_results.append(truth[ind,1])
				results_write.write(i)
				results_write.write("\t")
				if sum(dict_finder[i]) >= 1:
					prediction_results.append("1")
					results_write.write("1")
					results_write.write("\n")

				
				else:
					prediction_results.append("0")
					results_write.write("0")
					results_write.write("\n")

		elif len(dict_finder[i]) < 2:
			print(dict_finder[i])
		else:
			print(i, dict_finder[i])

	results_write.close()

	print("mcc results")
	print(matthews_corrcoef(correct_results, prediction_results) )

	print(correct_results)
	print(prediction_results)

	###confussion matrix
	tn, fp, fn, tp = confusion_matrix(correct_results, prediction_results).ravel()

	print("sensitivity", float(tp)/(tp + fn))
	print("specificity", float(tn)/(tn + fp))

	print(classification_report(correct_results, prediction_results, labels=["0", "1"]))

	correct_results2 = []
	for each in correct_results:
		correct_results2.append(int(each))

	prediction_results2 = []
	for each in prediction_results:
		prediction_results2.append(int(each))

	print("f1_score", f1_score(correct_results2, prediction_results2))

if __name__ == '__main__':
    main()	
