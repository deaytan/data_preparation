#!/usr/bin/env/python
import getopt, sys
import warnings


if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]

unixOptions = "p:s:h"
gnuOptions = ["path=","space=", "help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-p --path = Path to assemblies")
		print("-s --space = Space between the assemblies")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():

	##import the packages

	import re
	import numpy as np
	import os

	for currentArgument, currentValue in arguments:
			if currentArgument in ("-p","--path"):
				input_path = str(currentValue)
			if currentArgument in ("-s", "--space"):
				spacer = int(currentValue)

	file_list = os.listdir("%s" % input_path)

	##be sure there is no undesired file
	files = []
	for each in file_list:
		if ".fna" in each:
			files.append(each)

	new_file = open("all_strains_assembly.txt", "w")
	for i in files:
			open_scaf = open("%s/%s" % (input_path, i), "r")

			scaf = open_scaf.readlines()
		
			##write the header in the file
			new_file.write(">%s" % i)
			new_file.write("\n")
		
			##merge the assemblies with gaps, should be greater than the k-mer size
			for each in scaf:
				if ">" not in each:
					each_new = each.replace("\n","")
					new_file.write(each_new)
				else:
					new_file.write(str("N"*spacer))
					
			new_file.write("\n")

	new_file.close()

if __name__ == '__main__':
    main()	