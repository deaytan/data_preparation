#!/usr/bin/env/python
import getopt, sys
import warnings


if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]

unixOptions = "p:h"
gnuOptions = ["points=","help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-p --points = PointFinder results")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():

	##this script analyses all the pointfinder results 
	import os
	import collections
	import re
	import numpy as np

	for currentArgument, currentValue in arguments:
			if currentArgument in ("-p","--points"):
				input_path = currentValue

	###sample list desired to include into the dataset###
	file_dir = os.listdir("%s" % input_path)

	##be sure there is no undesired file
	points = []
	for each in file_dir:
		if ".sh" not in each and ".py" not in each and ".txt" not in each and ".png" not in each:
			points.append(each)

	dict_mutations = collections.defaultdict(list)
	all_samples = collections.defaultdict(list)
	all_aa = []
	for sample in points:
		list_point = os.listdir("%s/%s" % (input_path, sample))
		for item in list_point:
			if ".tsv" in item:
				data_tsv = open("%s/%s/%s" % (input_path, sample,item), "r")
				tsv = data_tsv.readlines()
				temp_dict = collections.defaultdict(list)
				for each in tsv[1:]:
					each = each.replace(" promotor", "")
					each = each.replace(" promoter", "")
				splitted1 = each.split("\t")[0]
				splitted = splitted1.split(" ")[0:3]
				splitted2 = each.split("\t")[1]
				splitted3 = each.split("\t")[2]
				splitteda = splitted2.split(" ")[0]
				splittedb = splitted2.split(" ")[2]
				if str(splitted[1][-1]) == "n":
					print(each)
				if "del" not in each and "ins" not in each:
					if re.search('\d', splitted[1]):
						if splitted[1][0] == "p":
							position = re.findall("(\-?\d+)", splitted[1])
							mutation = str(position[0]) + "_" + str(splitted[1][-1])
							all_aa.append(str(splitted[1][-1]))
							
						elif splitted[1][0] == "n" or splitted[1][0] == "r":
							position = re.findall("(\-?\d+)", splitted[1])
							mutation = str(position[0]) + "_" + str(splitted[1][-1]).lower()
							all_aa.append(str(splitted[1][-1]).lower())
							
						else:
							print("unrecognozed definition")
							print(splitted[1])		
					else:
						if splitted[2][0] == "p":
							position = re.findall("(\-?\d+)", splitted[2])
							mutation = str(position[0]) + "_" + str(splitted[2][-1])
							all_aa.append(str(splitted[2][-1]))
							
						elif splitted[2][0] == "n" or splitted[1][0] == "r":
							position = re.findall("(\-?\d+)", splitted[2])
							mutation = str(position[0]) + "_" + str(splitted[2][-1]).lower()
							all_aa.append(str(splitted[2][-1]).lower())
							
						else:
							print("unrecognozed definition")
							print(splitted[1])	
					dict_mutations[splitted[0].lower()].append(position[0])
					temp_dict[splitted[0].lower()].append(mutation)
					if all_aa[-1] == "l":
						print("problem")
						print(each)
						print(splitteda)
						print(splittedb)
				elif "*" in each:
					position = re.findall("(\-?\d+)", splitted[1])	
					mutation = str(position[0]) + "_" + "*"
					dict_mutations[splitted[0].lower()].append(position[0])
					temp_dict[splitted[0].lower()].append(mutation)
					all_aa.append("*")
					
				elif "del" in each:
					position = re.findall("(\-?\d+)", splitted[1])
					if len(position) > 1:
						for pos in range(int(position[0]), int(position[1])+1):
							mutation = str(pos) + "_" + "d"
							dict_mutations[splitted[0].lower()].append(str(pos))
							temp_dict[splitted[0].lower()].append(mutation)
					else:
						mutation = str(position[0]) + "_" + "d"
						dict_mutations[splitted[0].lower()].append(position[0])
						temp_dict[splitted[0].lower()].append(mutation)
					all_aa.append("d")
					
					
				elif "ins" in each:
					position = re.findall("(\-?\d+)", splitted[1])
					mutation = str(position[0]) + "_" + "i"
					dict_mutations[splitted[0].lower()].append(position[0])
					temp_dict[splitted[0].lower()].append(mutation)
					all_aa.append("i")
					
				else:
					print("unexpected error")
				all_samples[sample].append(temp_dict)
				




		
	results = open("pointfinder_chr_mutations.txt", "w")

	dict_mutations2 = collections.defaultdict(list)

	total = []
	for each in dict_mutations:
		uniq_res = list(set(list(dict_mutations[each])))
		dict_mutations2[each].append(uniq_res)
		for i in uniq_res:
			total.append(i)

	all_aa = list(set(all_aa)) ##unique variations

	 
	for k in all_samples:
		results.write(str(k))
		results.write("\t")
		for g in dict_mutations2:  
			if g in list(all_samples[k][0]):
				for each in dict_mutations2[g][0]:
					if each in [i.split("_")[0] for i in all_samples[k][0][g]]:
						index_list = []
						for x in all_samples[k][0][g]:
							index_list.append(x.split("_")[0])
						target_aa = index_list.index(each)
						aa = all_samples[k][0][g][target_aa][-1]
						aa_in = all_aa.index(aa)
						first = len(all_aa) - int(aa_in) - 1
						if aa_in != 0 and aa_in != len(all_aa) -1:
							for a in range(0,aa_in):
								results.write("-1")
								results.write("\t")
							results.write("1")
							results.write("\t")
							for b in range(0, first):
								results.write("-1")
								results.write("\t")
						if aa_in == 0:
							results.write("1")
							results.write("\t")
							for i in range(0, len(all_aa)-1):
								results.write("-1")
								results.write("\t")
						if aa_in == len(all_aa) -1:
							for i in range(0, len(all_aa)-1):
								results.write("-1")
								results.write("\t")
							results.write("1")
							results.write("\t")							
					else:
						for i in range(0, len(all_aa)):
							results.write("-1")
							results.write("\t")
			else:
				if g in dict_mutations2:
					for f in dict_mutations2[g][0]:
						for i in range(0, len(all_aa)):
							results.write("-1")
							results.write("\t")
		results.write("\n")
		
	results.close()

if __name__ == '__main__':
    main()
